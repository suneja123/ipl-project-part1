
import http from "http";
import fs from "fs";

const port=process.env.PORT || 3000

const server = http.createServer((req, res) => {
  // we can access HTTP headers
  console.log(req.url);
  if (req.method == "GET") console.log("request made");
  //console.log(req.headers.host);

    if(req.url.endsWith('.json')){
        let data = fs.readFileSync(`output/${req.url}`);
          if (data) {
            res.setHeader("Content-Type", "application/json");
            res.end(data);
          }  
    }

  try {
    let urlData = req.url.split("/");
    console.log(urlData,':urlData');
    switch (urlData[1]) {
      case "html":
        {
          let data = fs.readFileSync("index.html");
          if (data) {
           // console.log(data);
            res.writeHead(200, 'Content-Type', 'text/html');
            res.end(data);
          }else{
              Console.log(`error while reading html`)
          }
        }
        break;
      case "css":
        {
          let data = fs.readFileSync("style.css");
          if (data) {
            res.setHeader("Content-Type", "text/css");
            res.end(data);
          }
        }
        break;

      case "script.js":
        {
            console.log('came inside script');
           let data = fs.readFileSync("./script.js");
           if (data) {
            res.setHeader("Content-Type", "application/javascript");
            res.end(data);
          }
        }
        break;
        
    }
  } catch (error) {
    res.writeHead(400, { "Content-Type": "text/plain" });
    res.end(JSON.stringify(error.message));
  }
});

server.listen(port, () => {
  console.log("litsen" + server.address().port);
});
server.on("error", (err) => {
  console.log(err.message);
});
// server.listent(8000, "localhost", () => {
//   console.log("listning");
// });
// server.removeListener(8000,err=>{
//   console.log(err)
// })
// setTimeout(()=>{
//   server.close(err=>{
//     console.log(err);
//   });
// },60000)