//problem 1 :Number of matches played per year for all the years in IPL.
function matchesPlayedByPerYear(delivery, match) {
    let matchPerYear = {};
    matchPerYear = match.reduce((tempObj, matchObj) => {
        if (tempObj[matchObj.season]) {
            tempObj[matchObj.season] += 1;
        }
        else {
            tempObj[matchObj.season] = 1;
        }
        return tempObj;

    }, {})
    return matchPerYear;

}

//problem 2 :Number of matches won per team per year in IPL.
function matchesWonPerTeamPerYear(delivery, match) {
    let machesWonByTeam = match.reduce((machesWonByTeam, obj) => {

        if (machesWonByTeam.hasOwnProperty(obj.winner)) {
            //console.log(obj.winner);
            if (machesWonByTeam[obj.winner][obj.season]) {
                machesWonByTeam[obj.winner][obj.season] += 1;
            }
            else {
                machesWonByTeam[obj.winner][obj.season] = 1;
            }
        }
        else {
            machesWonByTeam[obj.winner] = {};
            machesWonByTeam[obj.winner][obj.season] = 1;
        }
        return machesWonByTeam;
    }, {})
    return machesWonByTeam;

}
//problem 3 :Extra runs conceded per team in the year 2016
function extraRunsConcededPerTeam(delivery, match) {
    let match2016 = [];
    let delivery2016 = [];
    let extraRunConcededTeam = {};
    match2016 = match.filter((obj) => {
        return obj.season === '2016'
    }).map(obj => obj.id)

    delivery2016 = delivery.filter(obj => {
        return match2016.includes(obj.match_id);
    }).map(obj => obj)


    extraRunConcededTeam = delivery2016.reduce((extraRunConcededTeam, deliveryObj) => {
        if (extraRunConcededTeam.hasOwnProperty(deliveryObj.bowling_team)) {
            extraRunConcededTeam[deliveryObj.bowling_team] += parseInt(deliveryObj.extra_runs)
        }
        else {
            extraRunConcededTeam[deliveryObj.bowling_team] = parseInt(deliveryObj.extra_runs)
        }
        return extraRunConcededTeam;

    }, {})
    return extraRunConcededTeam;
}
//problem 4 :Top 10 economical bowlers in the year 2015
function top10EconomicalBowlers2015(delivery, match) {
    let match2015 = [];
    let EconomicalBowlers2015 = {};

    match2015 = match.filter((obj) => {
        return obj.season === '2015'
    })

    match2015.forEach(matchObj => {
        delivery.forEach(deliveryObj => {
            //console.log(matchObj.id +" "+ deliveryObj.match_id)
            if (matchObj.id === deliveryObj.match_id) {
                if (EconomicalBowlers2015.hasOwnProperty(deliveryObj.bowler)) {

                    EconomicalBowlers2015[deliveryObj.bowler].economi += parseInt(deliveryObj.total_runs);
                    EconomicalBowlers2015[deliveryObj.bowler].deli += 1;
                    if (deliveryObj.extra_runs !== '0') {
                        EconomicalBowlers2015[deliveryObj.bowler].economi -= +deliveryObj.extra_runs;
                        EconomicalBowlers2015[deliveryObj.bowler].deli -= 1;
                    }

                }
                else {

                    EconomicalBowlers2015[deliveryObj.bowler] = {};
                    EconomicalBowlers2015[deliveryObj.bowler].economi = +deliveryObj.total_runs;
                    EconomicalBowlers2015[deliveryObj.bowler].deli = 1;
                    if (deliveryObj.extra_runs !== '0') {
                        EconomicalBowlers2015[deliveryObj.bowler].economi -= +deliveryObj.extra_runs;
                        EconomicalBowlers2015[deliveryObj.bowler].deli -= 1;
                    }
                }
            }
        })

    })
    let top10Economi = [];
    for (let key in EconomicalBowlers2015) {
        // console.log(EconomicalBowlers2015[key].deli);
        let over;
        over = EconomicalBowlers2015[key].deli / 6;
        EconomicalBowlers2015[key].economi /= over;
        top10Economi.push([key, EconomicalBowlers2015[key].economi])


    }
    top10Economi.sort(function (a, b) {
        return a[1] - b[1];
    })

    top10Economi = top10Economi.slice(0, 10);

    return top10Economi;
}

//problem 1:Find the number of times each team won the toss and also won the match



function winnerOfTossAndMatch(matchData) {

    let tossAndMatchWinner = matchData.filter(obj => {
        return obj.toss_winner === obj.winner
    })
    console.log(tossAndMatchWinner);
    let result = tossAndMatchWinner.reduce((tempObj, obj) => {
        if (tempObj.hasOwnProperty(obj.winner)) {
            tempObj[obj.winner] += 1;
        }
        else {
            tempObj[obj.winner] = 1;
        }

        return tempObj;

    }, {})
    return result;
}
//problem 2 :Find a player who has won the highest number of Player of the Match awards for each season

function awardedPlayer(match) {
    let winnerInEachSeason = {};
    match.forEach(matchObj => {
        if (winnerInEachSeason.hasOwnProperty(matchObj.season)) {
            if (winnerInEachSeason[matchObj.season].hasOwnProperty(matchObj.player_of_match) && matchObj.player_of_match !== '') {
                winnerInEachSeason[matchObj.season][matchObj.player_of_match] += 1;
            }
            else {
                winnerInEachSeason[matchObj.season][matchObj.player_of_match] = 1;

            }
        }
        else {
            winnerInEachSeason[matchObj.season] = {};
            winnerInEachSeason[matchObj.season][matchObj.player_of_match] = 1;
        }

    })

    for (let seosonKey in winnerInEachSeason) {
        let winnerKey = Object.keys(winnerInEachSeason[seosonKey])
        // console.log(winnerInEachSeason[seosonKey][winnerKey[0]]);
        let max = winnerInEachSeason[seosonKey][winnerKey[0]];
        let maxIndex = 0;
        for (let i = 0; i < winnerKey.length - 1; i++) {
            if (max >= winnerInEachSeason[seosonKey][winnerKey[i + 1]]) {

                delete winnerInEachSeason[seosonKey][winnerKey[i + 1]]

            } else {
                max = winnerInEachSeason[seosonKey][winnerKey[i + 1]];
                // console.log(max + " " + seosonKey);
                delete winnerInEachSeason[seosonKey][winnerKey[maxIndex]];
                maxIndex = i + 1;
            }
        }

    }

    return winnerInEachSeason;

}

// Find the strike rate of a batsman for each season
function strikeRateOfBatsman(deliveriesData, matchData) {
    let strike = {};

    uniqueSeasonAndId = matchData.reduce((tempObj, matchObj) => {
        tempObj[matchObj.id] = matchObj.season;
        return tempObj;
    }, {})
    let strikeRateofBatsmanObj = deliveriesData.reduce((strikeRateofBatsmanObj, deliveryData) => {
        if (strikeRateofBatsmanObj.hasOwnProperty(deliveryData.batsman)) {
            if (strikeRateofBatsmanObj[deliveryData.batsman].hasOwnProperty(uniqueSeasonAndId[deliveryData.match_id])) {
                strikeRateofBatsmanObj[deliveryData.batsman][uniqueSeasonAndId[deliveryData.match_id]]["runs"] += +deliveryData.batsman_runs;
                strikeRateofBatsmanObj[deliveryData.batsman][uniqueSeasonAndId[deliveryData.match_id]]["bowls"] += 1;


            } else {
                strikeRateofBatsmanObj[deliveryData.batsman][uniqueSeasonAndId[deliveryData.match_id]] = { "runs": +deliveryData.batsman_runs, "bowls": 1 };
            }

        } else {
            let tempObj = {};
            tempObj[uniqueSeasonAndId[deliveryData.match_id]] = { "runs": +deliveryData.batsman_runs, "bowls": 1 }
            strikeRateofBatsmanObj[deliveryData.batsman] = tempObj;
        }
        return strikeRateofBatsmanObj;
    }, {})


    for (let batsman in strikeRateofBatsmanObj) {
        for (let season in strikeRateofBatsmanObj[batsman]) {
            // console.log(strikeRateofBatsmanObj[batsman][season]["runs"] +" "+strikeRateofBatsmanObj[batsman][season]["bowls"])
            strikeRateofBatsmanObj[batsman][season] = (strikeRateofBatsmanObj[batsman][season]["runs"] / strikeRateofBatsmanObj[batsman][season]["bowls"]) * 100;
        }
    }
    return strikeRateofBatsmanObj;
}
// problem 4 : Find the highest number of times one player has been dismissed by another player
function highestTimeDissmissedPlayer(deliveriesData, matchData) {
    let playerDismissedCount = {}
    playerDismissedCount = deliveriesData.reduce((tempObj, deliveryObj) => {

        if (deliveryObj["player_dismissed"] !== "") {
            if (tempObj.hasOwnProperty(deliveryObj.player_dismissed)) {

                if (tempObj[deliveryObj.player_dismissed].hasOwnProperty(deliveryObj.bowler)) {
                    tempObj[deliveryObj.player_dismissed][deliveryObj.bowler] += 1;
                }
                else {
                    tempObj[deliveryObj.player_dismissed][deliveryObj.bowler] = 1;
                }
            }
            else {
                tempObj[deliveryObj.player_dismissed] = {};
                tempObj[deliveryObj.player_dismissed][deliveryObj.bowler] = 1;
            }
        }
        return tempObj;
    }, {})
    for (let batsman in playerDismissedCount) {
        let max = 0;
        for (let bowler in playerDismissedCount[batsman]) {

            if (max < playerDismissedCount[batsman][bowler]) {
                max = playerDismissedCount[batsman][bowler];

            }
        }
        playerDismissedCount[batsman] = max;
    }
    let max = 0;
    let player;
    for (let value in playerDismissedCount) {
        if (max < playerDismissedCount[value]) {
            max = playerDismissedCount[value];
            player = value;
        }
    }
    //console.log(playerDismissedCount);
    return player;

}
// problem 5:Find the bowler with the best economy in super overs

function BowlerWithTheBestEconomyInSuperOvers(deliveriesData, matchData) {
    let superOverData = deliveriesData.filter(obj => {
        if (obj.is_super_over !== '0') {
            return obj;
        }
    })

    superOverData = superOverData.reduce((tempObj, superOverDataObj) => {
        if (tempObj.hasOwnProperty(superOverDataObj.bowler)) {
            tempObj[superOverDataObj.bowler]["totalRun"] += +superOverDataObj.total_runs;
            tempObj[superOverDataObj.bowler]["totaldelivery"] += 1;
        }
        else {
            tempObj[superOverDataObj.bowler] = {};
            tempObj[superOverDataObj.bowler]["totalRun"] = +superOverDataObj.total_runs;
            tempObj[superOverDataObj.bowler]["totaldelivery"] = 1;
        }
        return tempObj;

    }, {})
    let highestEconomyBowler;
    let min = 100;

    for (let bowler in superOverData) {
        superOverData[bowler] = superOverData[bowler]["totalRun"] / (superOverData[bowler]["totaldelivery"] / 6)
        if (min > superOverData[bowler]) {
            min = superOverData[bowler]
            highestEconomyBowler = bowler;
        }
    }
    return highestEconomyBowler;

}
// in 2017 boundries hit by kohli count;
function playerWhoHitBoundries(deliveriesData, matchData) {
    let match2017Data = matchData.filter(matchobj => {
        return matchobj.season === "2017"
    }).map(obj => obj.id);
    // console.log(match2017Data);

    let hitBoundryCount = deliveriesData.filter((deliveryObj) => {
        return (match2017Data.includes(deliveryObj.match_id) && deliveryObj.batsman === "V Kohli") && (deliveryObj.batsman_runs == 4 || deliveryObj.batsman_runs == 6)
    })
        .reduce((tempObj, deliveryObj) => {
            if (tempObj.hasOwnProperty(deliveryObj.batsman_runs)) {
                tempObj[deliveryObj.batsman_runs] += 1;

            }
            else {
                tempObj[deliveryObj.batsman_runs] = 1;
            }
            return tempObj;
        }, {})

    return hitBoundryCount;

}
function top10BatsmanInEachSeason(deliveriesData, matchData) {
    let uniqueSeasonAndId = matchData.reduce((tempObj, matchObj) => {
        tempObj[matchObj.id] = matchObj.season;
        return tempObj;

    }, {})

    let batsmanAndScore = deliveriesData.reduce((tempObj, deliveryObj) => {
        //console.log((uniqueSeasonAndId[deliveryObj.match_id]));
        if (tempObj[uniqueSeasonAndId[deliveryObj.match_id]]) {
            if (tempObj[uniqueSeasonAndId[deliveryObj.match_id]].hasOwnProperty([deliveryObj.batsman])) {
                tempObj[uniqueSeasonAndId[deliveryObj.match_id]][deliveryObj.batsman] += +deliveryObj.batsman_runs;

            }
            else {
                tempObj[uniqueSeasonAndId[deliveryObj.match_id]][deliveryObj.batsman] = +deliveryObj.batsman_runs;

            }

        }
        else {
            tempObj[uniqueSeasonAndId[deliveryObj.match_id]] = {};
            if (deliveryObj.batsman_runs !== '0')
                tempObj[uniqueSeasonAndId[deliveryObj.match_id]][deliveryObj.batsman] = +deliveryObj.batsman_runs;
            //console.log(tempObj);
        }
        return tempObj;

    }, {})


    for (let season in batsmanAndScore) {
        let entries = Object.entries(batsmanAndScore[season]);

        let sorted = entries.sort((a, b) => a[1] - b[1]);

        batsmanAndScore[season] = (sorted.slice(sorted.length - 10)).reverse();
    }
    return batsmanAndScore;
}
function playerOutByCaught2010(deliveriesData, matchData) {
    let Match2010 = matchData.filter(obj => obj.season === "2010").map(obj => obj.id);
    let PlaysdismissedByCaught = deliveriesData.filter(obj => Match2010.includes(obj.match_id))


        .reduce((tempObj, deliveryObj) => {
            if (deliveryObj.player_dismissed !== "" && deliveryObj.dismissal_kind === "caught") {
                if (tempObj.hasOwnProperty(deliveryObj.fielder)) {
                    tempObj[deliveryObj.fielder] += 1;
                }
                else {
                    tempObj[deliveryObj.fielder] = 1;
                }
            }
            return tempObj;
        }, {})
    return PlaysdismissedByCaught;
}

function overTakenByJJIn2010(deliveriesData, matchData) {
    let balls = 0;
    let Match2010 = matchData.filter(obj => obj.season === "2016").map(obj => obj.id);
    let deliveryByJj = deliveriesData.filter(obj => (Match2010.includes(obj.match_id)
        && obj["bowler"] === "JJ Bumrah") && obj.extra_runs === "0")
    //console.log(deliveryByJj)
    deliveryByJj.forEach(element => {
        balls += 1;
    });
    //console.log(balls)
    return balls / 6;
}


function centuryInEachMatch2010(deliveriesData, matchData) {

    let Match2010 = matchData.filter(obj => obj.season === "2010").map(obj => obj.id);
    let batsmanCenturys = deliveriesData.filter(obj => Match2010.includes(obj.match_id))
        .reduce((tempObj, deliveryObj) => {

            if (tempObj.hasOwnProperty(deliveryObj.match_id)) {
                if (tempObj[deliveryObj.match_id].hasOwnProperty(deliveryObj.batsman)) {

                    tempObj[deliveryObj.match_id][deliveryObj.batsman] += +deliveryObj.batsman_runs

                }
                else {
                    tempObj[deliveryObj.match_id][deliveryObj.batsman] = +(deliveryObj.batsman_runs)
                }

            }
            else {
                tempObj[deliveryObj.match_id] = {}
                tempObj[deliveryObj.match_id][deliveryObj.batsman] = +(deliveryObj.batsman_runs);
            }

            return tempObj;
        }, {})
        for(let id in batsmanCenturys)
        {
           
            for(let batsman in batsmanCenturys[id])
            {
                if(batsmanCenturys[id][batsman] < 50)
                {
                    delete batsmanCenturys[id][batsman]
                }
            }
        }
    return batsmanCenturys;
}
    
function extraRunsConcededPerTeam(){
    
}
   


module.exports = {
    matchesPlayedByPerYear, matchesWonPerTeamPerYear,
    extraRunsConcededPerTeam, top10EconomicalBowlers2015,
    winnerOfTossAndMatch, awardedPlayer, strikeRateOfBatsman,
    highestTimeDissmissedPlayer, BowlerWithTheBestEconomyInSuperOvers,
    playerWhoHitBoundries, top10BatsmanInEachSeason, playerOutByCaught2010, overTakenByJJIn2010,
    centuryInEachMatch2010
}
