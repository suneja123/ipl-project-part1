//console.log('script.js loaded');

// function fetchData(path) {
//   fetch(path)
//     .then((response) => response.json())
//     .then((data) => {
//       return data;
//     })
//     .catch((err) => {
//       return err;
//     });
// }

//1
function matchesPlayedByPerYear() {
  fetch("./problem1.json")
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      let arr = [];
      for (let key in data) {
        arr.push([key, data[key]]);
      }

      Highcharts.chart("Matches_Played_Per_Year", {
        chart: {
          type: "column",
        },
        title: {
          text: "Matches Played By Per Team Per Year ",
        },
        subtitle: {
          text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>',
        },
        xAxis: {
          type: "category",
          labels: {
            rotation: -45,
            style: {
              fontSize: "13px",
              fontFamily: "Verdana, sans-serif",
            },
          },
        },
        yAxis: {
          min: 0,
          title: {
            text: "Match Score",
          },
        },
        legend: {
          enabled: false,
        },
        tooltip: {
          pointFormat: "Match played : <b>{point.y:.1f} </b>",
        },
        series: [
          {
            name: "Population",
            data: arr,
            dataLabels: {
              enabled: true,
              rotation: -90,
              color: "#FFFFFF",
              align: "right",
              format: "{point.y:.1f}", // one decimal
              y: 10, // 10 pixels down from the top
              style: {
                fontSize: "13px",
                fontFamily: "Verdana, sans-serif",
              },
            },
          },
        ],
      });
    })
    .catch((err) => {
      console.log(err);
    });
}
matchesPlayedByPerYear();


//2
function matchesWonPerTeamPerYear() {
  fetch("./problem2.json")
    .then((response) => response.json())
    .then((data) => {

			let team = Object.keys(data)
      console.log(data,':matchperyear');
		
			let years = [];
			for (const team in data) {
				for (const year in data[team] ) {
					if (years.indexOf(year) == -1) {
						years.push(year);
					}
				}
			}
		
			let arr = [];
			for (const year of years.sort()) {
				const temp = [];
				for (const team in data) {
					if(data[team][year] !== undefined)
					{
						temp.push(data[team][year]);
					}
				}
				arr.push({
					name: year,
					data: temp,
				});
			}
		 


			Highcharts.chart('Matches_Won_Per_Year', {
				chart: {
						type: 'column'
				},
				title: {
						text: 'Monthly Average Rainfall'
				},
				subtitle: {
						text: 'Source: WorldClimate.com'
				},
				xAxis: {
						categories: team,
						crosshair: true
				},
				yAxis: {
						min: 0,
						title: {
								text: 'Rainfall (mm)'
						}
				},
				tooltip: {
						headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
						pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
								'<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
						footerFormat: '</table>',
						shared: true,
						useHTML: true
				},
				plotOptions: {
						column: {
								pointPadding: 0.2,
								borderWidth: 0
						}
				},
				series: arr
		});
    })
    .catch((err) => console.log(err));
}
matchesWonPerTeamPerYear();

//3
function extraRunsConcededPerTeam() {
  fetch("./problem3.json")
    .then((response) => response.json())
    .then((data) => {
      let arr = [];
      for (let key in data) {
        arr.push([key, data[key]]);
      }

      Highcharts.chart("extra_run", {
        chart: {
          type: "column",
        },
        title: {
          text: "Matches Won By Per Team  ",
        },
        subtitle: {
          text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>',
        },
        xAxis: {
          type: "category",
          labels: {
            rotation: -45,
            style: {
              fontSize: "13px",
              fontFamily: "Verdana, sans-serif",
            },
          },
        },
        yAxis: {
          min: 0,
          title: {
            text: "Match Score",
          },
        },
        legend: {
          enabled: false,
        },
        tooltip: {
          pointFormat: "Match Won : <b>{point.y:.1f} </b>",
        },
        series: [
          {
            name: "Population",
            data: arr,
            dataLabels: {
              enabled: true,
              rotation: -90,
              color: "#FFFFFF",
              align: "right",
              format: "{point.y:.1f}", // one decimal
              y: 10, // 10 pixels down from the top
              style: {
                fontSize: "13px",
                fontFamily: "Verdana, sans-serif",
              },
            },
          },
        ],
      });
    })
    .catch((err) => console.log(err));
}
extraRunsConcededPerTeam();
