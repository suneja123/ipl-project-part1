const fs = require('fs');
const { matchesPlayedByPerYear } = require('./ipl');

const { matchesWonPerTeamPerYear } = require('./ipl');
const { extraRunsConcededPerTeam } = require('./ipl');
const { top10EconomicalBowlers2015 } = require('./ipl');
const { winnerOfTossAndMatch } = require('./ipl');
const { awardedPlayer } = require('./ipl');
const { strikeRateOfBatsman } = require('./ipl');
const { highestTimeDissmissedPlayer } = require('./ipl');
const { BowlerWithTheBestEconomyInSuperOvers } = require('./ipl');
const { playerWhoHitBoundries} = require('./ipl');
const {top10BatsmanInEachSeason} = require('./ipl');
const {playerOutByCaught2010} = require('./ipl');
const {overTakenByJJIn2010} = require('./ipl');

const {centuryInEachMatch2010} = require('./ipl');



const matchesFilePath = "src/data/matches.json";
const delevriFilePath = "src/data/deliveries.json";

try {
  const deliveryString = fs.readFileSync(delevriFilePath);
  const deliveriesData = JSON.parse(deliveryString);
  //console.log(deliveriesData);

  const matchString = fs.readFileSync(matchesFilePath);
  const matchObject = JSON.parse(matchString);
  //console.log(matchObject);




  // let problem1 = matchesPlayedByPerYear(deliveriesData, matchObject);
  // let problem2 = matchesWonPerTeamPerYear(deliveriesData, matchObject);
  // let problem3 = extraRunsConcededPerTeam(deliveriesData, matchObject);
  // let problem4 = top10EconomicalBowlers2015(deliveriesData, matchObject);

  // let advanceProblem1 = winnerOfTossAndMatch(matchObject);
  // let advanceProblem2 = awardedPlayer(matchObject);
  // let advanceProblem3 = strikeRateOfBatsman(deliveriesData, matchObject);
  // let advanceProblem4 = highestTimeDissmissedPlayer(deliveriesData, matchObject);
  // let advanceProblem5 = BowlerWithTheBestEconomyInSuperOvers(deliveriesData, matchObject);

  //console.log(playerWhoHitBoundries(deliveriesData, matchObject));
  //console.log(playerOutByCaught2010(deliveriesData, matchObject));
  console.log(centuryInEachMatch2010(deliveriesData, matchObject));




//   fs.writeFile("src/public/output/problem1.json", JSON.stringify(problem1,null,2),
//     (err) => {
//       if (err) console.log(err);
//     })
//   fs.writeFile("src/public/output/problem2.json", JSON.stringify(problem2,null,2),
//     (err) => {
//       if (err) console.log(err);
//     })
//   fs.writeFile("src/public/output/problem3.json", JSON.stringify(problem3,null,2),
//     (err) => {
//       if (err) console.log(err);
//     })
//   fs.writeFile("src/public/output/problem4.json", JSON.stringify(problem4,null,2),
//     (err) => {
//       if (err) console.log(err);
//     })

//   fs.writeFile("src/public/output/advanceProblem1.json", JSON.stringify(advanceProblem1,null,2),
//     (err) => {
//       if (err) console.log(err);
//     })
//   fs.writeFile("src/public/output/advanceProblem2.json", JSON.stringify(advanceProblem2,null,2),
//     (err) => {
//       if (err) console.log(err);
//     })
//   fs.writeFile("src/public/output/advanceProblem3.json", JSON.stringify(advanceProblem3,null,2),
//     (err) => {
//       if (err) console.log(err);
//     })
//   fs.writeFile("src/public/output/advanceProblem4.json", JSON.stringify(advanceProblem4,null,2),
//     (err) => {
//       if (err) console.log(err);
//     })
//     fs.writeFile("src/public/output/advanceProblem5.json", JSON.stringify(advanceProblem5,null,2),
//     (err) => {
//       if (err) console.log(err);
//     })

}
catch (err) {
  console.log(err);

}
